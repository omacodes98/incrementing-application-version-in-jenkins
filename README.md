# Incrementing Application Version 

Dynamically Increment Application version in Jenkins Pipeline

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Configure CI step: Increment patch version

* Configure CI step: Build Java application and clean old artifacts

* Configure CI step: Build Image with dynamic Docker Image Tag

* Configure CI step: Push Image to private DockerHub repository

* Configure CI step: Commit version update of Jenkins back to Git repository

* Configure Jenkins pipeline to not trigger automatically on CI build commit to avoid commit loop

## Technologies Used 

* Jenkins 

* Docker 

* GitLab 

* Git 

* Java 

* Maven 

## Steps 

Step 1: Add plugins to jenkins file to enable incrementing of version 

[Adding Plugin to Jenkinsfile](/images/01_adding_pluging_to_allow_incrementing_of_version.png)

Step 2: Add readfile to read version in pomxml

[Adding readFile](/images/02_attaching_readFile_to_read_version_in_pomxml.png)

Step 3: Insert environmental variable in docker image 

[Environmental Var in Docker image](/images/03_inserting_env_var_in_docker_image.png)

Step 4: Check Dockerfile to make sure the name isn't hard coded 

[Dockerfile](/images/04_check_Dockerfile_to_make_sure_the_name_isnt_hard_coded.png)

Step 5: Put mvn clean package in Jenksinsfile to avoid errors in dockerfile when creating the docker image 

     mvn clean package 

[mvn clean package](/images/05_cleans_old_packages_to_avoid_errors_in_dockerfile.png)

Step 6: Change configuration on pipeline to test version incrementing 

[Changing Pipeline Config](/images/06_changing_config_on_mypipline_to_test_it.png)
[Test Successful](/images/07_increment_version_success.png)
[Version Update](/images/08_verison_update_change.png)
[Docker Hub](/images/09_docker_hub_repo.png)

Step 7: Add logic to Jenkinsfile to enable Jenkins to commit to gitlab 

[Jenkins commit logic](/images/10_commiting_from_jenkins_to_git_repo.png)

Step 8: Test changes to Jenkinsfile by pushing it to gitlab.

[Commit Version Jenkins](/images/11_commit_version_stage_got_added.png)
[Commit on GitLab](/images/12_jenkins_commit_on_gitlab.png)

Step 9: Step 8 will trigger and infinite loop which is caused by Jenkins commit. To change this you will need to download a plugin on jenkins called ignore committer strategy 

[Ignore Committer Strategy 1](/images/13_installed_ignore_committer_strategy.png)
[Ignore Committer Strategy 2](/images/13_installed_ignore_committer_strategy_1.png)

Step 10: Configure multibranch build strategies to ignore jenkins commit by putting in the email address that is in the Jenkinsfile logic for jenkins commit 

[Build strategies configuration](/images/14_configuring_build_strategies_to_ignore_jenkins_commits.png)

Step 11: Test configuration change by Triggering Job automatically, you will do this by pushing to gitlab repo 

[Automatic Triggr](/images/15_triggering_job_automatically.png)
[Stopped loop](/images/16_stopped_loop.png)
[Jenkins Commit on Gitlab](/images/17_commit_on_gitlab.png)


## Installation

Run $ apt install nodejs 

## Usage 

Run $ java -jar java-maven-app*.jar 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/incrementing-application-version-in-jenkins.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using npm test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/incrementing-application-version-in-jenkins

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.